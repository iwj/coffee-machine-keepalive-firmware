Firmware for a post-hoc modification to a coffee filter machine
===============================================================

Copyright 2023 Ian Jackson.
avr-hal Copyright many contributors.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program in the file GPL-3.
If not, see <http://www.gnu.org/licenses/>.

avr-hal subdirectory
--------------------

The `avr-hal/` subdirectory contains a *lightly modified*
version of the "Hardware Abstraction Layer for AVR microcontrollers"
which was itself made from "avr-devices".

That subdirectory is all dual licenced, MIT or Apache-2.0.

The original `avr-hal` project can be found here:
<https://github.com/Rahix/avr-hal>.

The file `avr-hal/examples/trinket/src/bin/trinket-blink.rs`
has been modified to
*include the toplevel file `program.rs` from outside `avr-hal`*.
(This words around some build system difficulties.)
