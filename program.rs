//! Main program
//!
//! Copyright 2023 Ian Jackson
//! SPDX-License-Identifier: GPL-3.0-or-later
//!
//! Plan is:
//!
//! Initially, press button once
//!
//! Then, wait 24 minutes.
//! Press button again, twice.
//!
//! Do the same three more times.
//! That amounts to a total time of 120 minutes,
//! when we include the coffee machine's own timeout.

// Board: Digispark (1mhz - no USB)  XXX how to do this?

pub type Ms = i32;

use super::*;
use void::{Void, ResultVoidErrExt as _};

const PRESS_DURATION: Ms = 200;
const PRESS_INTERVAL: Ms = 1000;
const RESET_EVERY: Ms = 23 * 60 * 1000;
const RESET_COUNT: u8 = 5;

// LED output
//
//   Within 5 seconds of pressing button, flash frenetically
//
//   The rest of the time, flash the reset count

const LED_COUNT_TIME_ON: Ms = 100;
const LED_COUNT_TIME_OFF: Ms = 200;
const LED_COUNT_TIME_BETWEEN: Ms = 2000;

const LED_FRENETIC_HALFPERIOD: Ms = 75;
const LED_FRENETIC_TIME: Ms = 5 * 1000;

type LedPin = Pin<Output, PB1>;
type OutPin = Pin<Output, PB0>;

struct State {
  /// Coffee machine (optoisolator), high = pressing button
  out: OutPin,
  ///
  led: LedPin,
  /// Low = drain power faster
  drain: Pin<OpenDrain, PB2>,
  ///
  clock: Clock,
}

struct Clock {
}

impl Clock {
  fn delay(&mut self, ms: Ms) {
    super::delay(ms);
  }
}

#[allow(dead_code)]
fn delay(_: DoNotCallDirectly) { }
enum DoNotCallDirectly { }

pub fn main(pins: Pins) -> ! {
    State {
      out: pins.d0.into_output(),
      led: pins.d1.into_output(),
      drain: pins.d2.into_opendrain_high(),
      clock: Clock { },
    }.run();
}

struct TrackingClock<'c> {
  remaining: Ms,
  clock: &'c mut Clock,
}

impl Clock {
  fn track_remaining(&mut self, remaining: Ms) -> TrackingClock<'_> {
    TrackingClock { clock: self, remaining }
  }
}

struct Expired;

impl<'c> TrackingClock<'c> {
  fn delay(&mut self, ms: Ms) -> Result<(), Expired> {
    if self.remaining <= ms {
      return Err(Expired);
    }
    self.remaining -= ms;
    self.clock.delay(ms);
    Ok(())
  }

  fn delay_rest(self) {
    self.clock.delay(self.remaining);
  }
}

mod frenetic {
  use super::*;

  pub struct Frenetic<'s> {
    state: &'s mut State,
    until_next: Ms,
  }

  impl State {
    pub fn with_frenetic<R, F>(&mut self, f: F) -> R
    where F: FnOnce(&mut Frenetic) -> R,
    {
      self.led.set_high();
      let mut frenetic = Frenetic {
        state: self,
        until_next: LED_FRENETIC_HALFPERIOD,
      };
      let r = f(&mut frenetic);
      frenetic.finish();
      r
    }
  }

  impl<'s> Frenetic<'s> {
    pub fn delay(&mut self, mut their_ms: Ms) {
      enum Earlier {
        Us,
        Them,
      }

      while their_ms > 0 {
        let (wait_now, who) = if self.until_next < their_ms {
          (self.until_next, Earlier::Us)
        } else {
          (their_ms, Earlier::Them)
        };

        if wait_now > 0 {
          self.until_next -= wait_now;
          their_ms        -= wait_now;
          self.state.clock.delay(wait_now);
        }
      
        match who {
          Earlier::Us => {
            self.state.led.toggle();
            self.until_next += LED_FRENETIC_HALFPERIOD;
          }
          Earlier::Them => {
          }
        }
      }
    }

    pub fn press_once(&mut self) {
      self.state.out.set_high();
      self.delay(PRESS_DURATION);
      self.state.out.set_low();
    }

    fn finish(self) {
      if self.state.led.is_set_high() {
        self.state.clock.delay(self.until_next);
      }
      self.state.led.set_low();
    }
  }
}

impl State {
  fn run(&mut self) -> ! {
    self.with_frenetic(|frenetic| {
      frenetic.press_once()
    });

    for iter in (1..=RESET_COUNT).rev() {
      let led = &mut self.led;
      let mut tclock = self.clock
        .track_remaining(RESET_EVERY - LED_FRENETIC_TIME);

      let Expired = (|| -> Result<Void, _> {
        loop {
          tclock.delay(LED_COUNT_TIME_BETWEEN/2)?;

          for _ in 1..=iter {
	    led.set_high();
	    tclock.delay(LED_COUNT_TIME_ON)?;
	    led.set_low();
	    tclock.delay(LED_COUNT_TIME_OFF)?;
          }

          tclock.delay(
            LED_COUNT_TIME_BETWEEN/2 - LED_COUNT_TIME_OFF
          )?;
        }
      })().void_unwrap_err();

      tclock.delay_rest();

      self.drain.set_low();
      self.with_frenetic(|frenetic| {
        frenetic.delay(LED_FRENETIC_TIME);
        frenetic.press_once();
        frenetic.delay(PRESS_INTERVAL);
        frenetic.press_once();
      });
      self.drain.set_high();
    }

    loop {
      self.clock.delay(RESET_EVERY);
    }
  }
}
