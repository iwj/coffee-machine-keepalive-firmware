// Copyright 2023 Ian Jackson
// Copyright avr-hal and avr-device contributors.
//
// SPDX-License-Identifier: MIT OR Apache-2.0

#![no_std]
#![no_main]

#![allow(unused_imports)]

use core::convert::TryInto as _;

use panic_halt as _;

#[path = "../../../../../program.rs"]
mod program;
use program::*;

use arduino_hal::Pins;
use arduino_hal::port::Pin;
use arduino_hal::port::mode::{self, *};
use attiny_hal::port::{PB0, PB1, PB2};

fn delay(mut ms: Ms) {
  use arduino_hal::delay_ms;

  // Correct for clock error
  // digispark is running at 16.5MHz
  // we're using wrong board definitions that say 8MHz
  ms = (ms << 1) + (ms >> 4);

  loop {
    if let Ok(ms) = ms.try_into() {
      delay_ms(ms);
      return;
    }
    let this = u16::MAX;
    ms -= Ms::from(this);
    delay_ms(this);
  }
}    

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    
    program::main(pins);
}
