# Copyright 2023 Ian Jackson
# SPDX-License-Identifier: GPL-3.0-or-later

CFLAGS= -Wall -Werror

all: avr-build rsimulate

NAILING_CARGO=nailing-cargo
CARGO=$(NAILING_CARGO)

AVR_SUBDIR=avr-hal
AVR_CARGO=IN_SUBDIR=examples/trinket $(NAILING_CARGO) -c --- ./in-avr-hal
# ^ add -Eu to update Csrgo.lock

AVR_TARGET_SUBDIR=target/in-avr-hal//avr-attiny85/release

MICRONUCLEUS ?= ../micronucleus/commandline/micronucleus

rsimulate-check:
	$(CARGO) check

rsimulate:
	$(CARGO) build

cargo-check:
	 $(AVR_CARGO) check --bin trinket-blink

cargo-build:
	 $(AVR_CARGO) build --release --bin trinket-blink

cargo-doc:
	 $(AVR_CARGO) doc

avr-build: cargo-build
	$(NAILING_CARGO) --just-run --- \
	avr-objcopy --output-target=ihex  \
		$(AVR_TARGET_SUBDIR)/trinket-blink.elf \
		$(AVR_TARGET_SUBDIR)/trinket-blink.hex
	$(NAILING_CARGO) --just-run --- \
		cat $(AVR_TARGET_SUBDIR)/trinket-blink.hex >avr.hex


install: avr-build
	$(MICRONUCLEUS) --run avr.hex
