//! Simulator harness
//!
//! Copyright 2023 Ian Jackson
//! SPDX-License-Identifier: GPL-3.0-or-later

#[path = "../../program.rs"]
mod program;
use program::*;

use core::mem;
use core::marker::PhantomData;
use derive_more::{Deref, DerefMut};
use paste::paste;
use std::sync::{Mutex, MutexGuard, OnceLock};

#[derive(Default)]
pub struct Pin<M, P> {
  mode: PhantomData<M>,
  pin: PhantomData<P>,
}

#[derive(Default)]
pub struct SimMode<const MO: char>;

#[derive(Default)]
pub struct SimPin<const PI: usize>;

type PB0 = SimPin<0>;
type PB1 = SimPin<1>;
type PB2 = SimPin<2>;

const N_PINS: usize = 6;

type UnsetMode = SimMode<'\0'>;
type Output    = SimMode<'o'>;
type OpenDrain = SimMode<'d'>;

#[derive(Default)]
pub struct Pins {
  pub d0: Pin<UnsetMode, PB0>,
  pub d1: Pin<UnsetMode, PB1>,
  pub d2: Pin<UnsetMode, PB2>,
}

fn default<T: Default>() -> T { Default::default() }

macro_rules! def_into_mode { { $mode:ident, $val:expr } => { paste!{
  fn [<into_ $mode:lower _with_level>]
    (self, level: bool)-> Pin<$mode, SimPin<PI>>
  {
    let mut sim = sim();
    let pin = &mut sim.core.pins[PI];
    pin.mode = $val;
    pin.level = Some(level);
    default()
  }
  pub fn [<into_ $mode:lower>](self) -> Pin<$mode, SimPin<PI>> {
    self.[<into_ $mode:lower _with_level>](false)
  }
  pub fn [<into_ $mode:lower _high>](self) -> Pin<$mode, SimPin<PI>> {
    self.[<into_ $mode:lower _with_level>](true)
  }
} } }

impl<const MO: char, const PI: usize> Pin<SimMode<MO>, SimPin<PI>> {
  def_into_mode!(Output,    'o');
  def_into_mode!(OpenDrain, 'd');
  pub fn set_high(&mut self) { self.set_pin_level(PI, true); }
  pub fn set_low (&mut self) { self.set_pin_level(PI, false); }
  pub fn is_set_high(&mut self) -> bool { self.is_set_pin_level(PI, true) }
  pub fn is_set_low (&mut self) -> bool { self.is_set_pin_level(PI, false) }
  pub fn set_pin_level(&mut self, pi: usize, level: bool) {
    sim().core.pins[pi].level = Some(level);
  }
  pub fn is_set_pin_level(&mut self, pi: usize, level: bool) -> bool {
    sim().core.pins[pi].level.unwrap() == level
  }
  pub fn toggle(&mut self) {
    match &mut sim().core.pins[PI].level {
      Some(level) => *level = !*level,
      None => panic!(),
    }
  }
}

#[derive(Default)]
struct SimPinState {
  mode: char,
  level: Option<bool>,
}

enum DisplayState {
  Compact {
    changed: bool,
  },
  Propor {
    last: Ms,
    unit: Ms,
    limit: Ms,
  },
}

impl Default for DisplayState {
  fn default() -> Self { DS::Compact { changed: true } }
}

use DisplayState as DS;

#[derive(Default)]
struct Sim {
  core: SimCore,
  ds: DisplayState,
  stop_at: Option<Ms>,
}

#[derive(Default)]
struct SimCore {
  now: Ms,
  pins: [SimPinState; N_PINS],
}

static SIM: OnceLock<Mutex<Sim>> = OnceLock::new();

fn sim_raw() -> MutexGuard<'static, Sim> {
  SIM.get_or_init(default).lock().unwrap()
}

fn sim() -> SimGuard {
  SimGuard(sim_raw())
}

#[derive(Deref, DerefMut)]
struct SimGuard(MutexGuard<'static, Sim>);

impl Drop for SimGuard {
  fn drop(&mut self) {
    match &mut self.ds {
      DS::Compact { changed } => *changed = true,
      DS::Propor { .. } => {},
    }
  }
}

fn pr_millis_mins(ms: Ms) {
  let remainder = ms % 60000;
  print!("{:7} {:4}:{:02}.{:03}",
	 ms,
	 ms / 60000,
	 remainder / 1000,
	 remainder % 1000);
}

impl SimCore {
  fn report1(&self, ms: Ms) {
    pr_millis_mins(ms);
    print!(" ");
  }

  fn report2(&self) {
    for p in &self.pins {
      print!("  ");
      match (p.mode, p.level) {
        ('\0', _) => print!("?"),
        ('o', Some(l)) => print!("{}", b".#"[l as usize] as char),
        ('d', Some(true)) => print!("i"),
        ('d', Some(false)) => print!("."),
        x => print!("XX {:?}", x),
      }
    }
    println!();
  }
}

fn delay(ms: Ms) {
  let mut sim = sim_raw();
  let sim = &mut *sim;
  let core = &mut sim.core;
  
  match &mut sim.ds {
    DS::Compact { changed } => {
      if mem::take(changed) {
        core.report1(core.now);
        print!("       ");
        core.report2();
      }
    }
    DS::Propor { .. } => {}
  }
  
  core.now += ms;

  match &mut sim.ds {
    DS::Compact { .. } => {}
    DS::Propor { last, unit, limit } => {
      let period = core.now - *last;
      if period < *unit {
        *last = core.now;
        core.report1(*last);
        print!("S {:05}", period);
        core.report2();
      } else if period <= *limit {
        let units = period / *unit;
        let remain = period % *unit;
        for u in 0..units {
	  *last += *unit;
	  if u==units-1 && remain > 0 {
	    core.report1(core.now);
	    print!("R {:5}", remain);
	  } else {
	    core.report1(*last);
	    print!("       ");
	  }
	  core.report2();
        }
      } else {
        print!("                   ");
        pr_millis_mins(period);
        println!();
      }
    }
  }

  if let Some(stop_at) = sim.stop_at {
    if sim.core.now >= stop_at {
      pr_millis_mins(stop_at);
      println!(" stopping (rust)");
      std::process::exit(0);
    }
  }
}

fn main(){
  {
    let mut sim = sim_raw();
    for arg in std::env::args().skip(1) {
      if arg == "-p" {
        sim.ds = DS::Propor {
          last: default(),
          unit: 100,
          limit: 10_000,
        }
      } else if arg == "-c" {
        sim.ds = default();
      } else if let Some(e) = arg.strip_prefix("-e") {
        sim.stop_at = Some(e.parse().unwrap());
      } else {
        panic!("bad argument {arg:?}");
      }
    }
  }

  program::main(default());
}
